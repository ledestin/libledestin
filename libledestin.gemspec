Gem::Specification.new do |s|
  s.name        = 'libledestin'
  s.version     = '0.0.1'
  s.date        = '2014-07-23'
  s.summary     = 'Useful stuff, private gem'
  s.authors     = ['Dmitry Maksyoma']
  s.email       = 'ledestin@gmail.com'
  s.licenses    = 'MIT'
  s.files       = `git ls-files`.split($\)
  s.test_files = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ['lib']
  s.homepage    = 'https://bitbucket.org/ledestin/libledestin'

  s.add_development_dependency 'rspec', '~> 3.0'
end
