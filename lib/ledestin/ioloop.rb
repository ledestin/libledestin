# Copyright (C) 2013, 2014 by Dmitry Maksyoma <ledestin@gmail.com>

require 'monitor'
require 'thread'

module IOLoop
  @rds, @wrs = {}, {}
  @rds.extend MonitorMixin
  @wrs.extend MonitorMixin
  @call_callback_on_unregister_read = true

  Thread.new { IOLoop.process }

  def self.call_callback_on_unregister_read
    @call_callback_on_unregister_read
  end

  def self.call_callback_on_unregister_read=(flag)
    @call_callback_on_unregister_read = flag
  end

  def self.process
    loop {
      begin
	rds, wrs = IO.select(rds_to_poll, wrs_to_poll, nil, 0.5)
      rescue IOError
	retry
      end

      process_rds rds
      process_wrs wrs
    }
  end

  def self.process_rds rds
    return unless rds

    rds.each { |io|
      begin
	@rds.synchronize {
	  if cb = @rds[io]
	    buf = io.read_nonblock 1024
	    #debug "read: #{buf.size}"
	    cb.call buf
	  end
	}
      rescue Errno::EAGAIN
      rescue Errno::EINTR
	retry
      rescue EOFError, Errno::ECONNRESET, Errno::ETIMEDOUT
	unregisterRead io
      end
    }
  end

  def self.process_wrs wrs
    return unless wrs

    wrs.each { |io|
      begin
	@wrs.synchronize {
	  next unless buf = @wrs[io]

	  until buf.empty?
	    wrote = io.write_nonblock buf
	    buf.slice! 0...wrote
	  end
	  @wrs[io] = nil
	}
      rescue Errno::EPIPE, Errno::ECONNRESET
	unregisterWrite io
      rescue Errno::EAGAIN
      rescue Errno::EINTR
	retry
      end
    }
  end

  def self.rds_to_poll
    @rds.synchronize {
      unregister_closed(@rds.keys) { |io|
	unregister_read io
      }
    }
  end

  def self.registered? io
    registered_rd?(io) || registered_wr?(io)
  end

  def self.registerRead io, &callback
    @rds.synchronize { @rds[io] = callback }
  end

  def self.registerWrite io
    @wrs.synchronize { @wrs[io] = nil }
  end

  def self.select_if_value_present hash
    hash.select { |k, v| v }.map { |k, v| k }
  end

  def self.send io, str
    @wrs.synchronize {
      if buf = @wrs[io]
	buf << str
      else
	@wrs[io] = str
      end
    }
  end

  def self.unregisterRead io
    cb = @rds.synchronize { @rds.delete io }
    cb.call nil if @call_callback_on_unregister_read && cb
  end

  def self.unregisterWrite io
    @wrs.synchronize { @wrs.delete io }
  end

  def self.wrs_to_poll
    @wrs.synchronize {
      unregister_closed(select_if_value_present @wrs) { |io|
	unregister_write io
      }
    }
  end

  class << self
    alias :register_write :registerWrite
    alias :register_read :registerRead
    alias :unregister_read :unregisterRead
    alias :unregister_write :unregisterWrite
  end

  private

  def self.registered_rd? io
    @rds.synchronize { @rds.has_key? io }
  end

  def self.registered_wr? io
    @wrs.synchronize { @wrs.has_key? io }
  end

  def self.unregister_closed ios, &b
    ios.each { |io|
      yield io if io.closed?
    }
  end
end
