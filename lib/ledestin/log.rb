#Copyright (C) 2013 by Dmitry Maksyoma <ledestin@gmail.com>

require 'thread'

# {{{1 Log
module Log
  @stdMutex = Mutex.new
  @level = :info

  def self.debug msg, out=:stdout
    @stdMutex.synchronize { outputStream(out).print 'debug: ', msg, "\n" } \
      if @level == :debug
  end

  def self.info msg, out=:stdout
    @stdMutex.synchronize { outputStream(out).puts msg }
  end

  def self.outputStream out
    case out
    when :stdout
      $stdout
    when :stderr
      $stderr
    else
      raise "#{out}: unknown output io"
    end
  end

  def self.level=(l)
    raise "#{l}: unknown level" unless l == :debug || l == :info
    @level = l
  end

  def debug msg, out=:stdout
    Log.debug msg, out
  end

  def info msg, out=:stdout
    Log.info msg, out
  end
end

