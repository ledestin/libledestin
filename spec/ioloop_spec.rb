require './lib/ledestin/ioloop'

STR = 'hi'
Thread.abort_on_exception = true

describe IOLoop do
  before :each do
    @rd, @wr = IO.pipe
  end

  context 'io is registered by' do
    it '#register_read' do
      IOLoop.register_read @rd
      expect(IOLoop.registered? @rd).to eq true
    end

    it '#register_write' do
      IOLoop.register_write @wr
      expect(IOLoop.registered? @wr).to eq true
    end
  end

  context 'io is unregistered by' do
    it '#unregister_read' do
      IOLoop.register_read @rd
      IOLoop.unregister_read @rd
      expect(IOLoop.registered? @rd).to eq false
    end

    it '#unregister_write' do
      IOLoop.register_write @wr
      IOLoop.unregister_write @wr
      expect(IOLoop.registered? @wr).to eq false
    end
  end

  it "read callback isn't called after io was unregistered" do
    callback_called = false
    IOLoop.register_read(@rd) { |buf| callback_called = !!buf }
    IOLoop.unregister_read @rd
    @wr.write 'hi'
    IOLoop.process_rds [@rd]
    expect(callback_called).to eq false
  end

  it '#send writes bytes into specified io' do
    IOLoop.register_write @wr
    IOLoop.send @wr, STR.clone
    rds, = select [@rd], nil, nil, 1
    expect(rds.first).to eq @rd
    expect(@rd.read STR.bytesize).to eq STR
  end

  context 'read callback' do
    it 'gets data written with #send' do
      rd_contents = :empty
      IOLoop.register_read(@rd) { |buf| rd_contents = buf }
      IOLoop.register_write @wr
      IOLoop.send @wr, STR.clone
      sleep 2
      expect(rd_contents).to eq STR
    end

    it 'yields nil, when @wr gets closed' do
      rd_contents = :empty
      IOLoop.register_read(@rd) { |buf| rd_contents = buf }
      @wr.close
      sleep 1
      expect(rd_contents).to be_nil
    end
  end

  it '@rd is unregistered when @wr is closed' do
    IOLoop.register_read(@rd) {}
    @wr.close
    sleep 2
    expect(IOLoop.registered? @rd).to eq false
  end

  it '@wr is unregistered when @rd is closed' do
    IOLoop.register_write @wr
    IOLoop.send @wr, STR.clone
    @rd.close
    sleep 2
    expect(IOLoop.registered? @wr).to eq false
  end

  it 'io closed from outside case is handled gracefully' do
    IOLoop.register_read(@rd) {}
    @rd.close
    expect { sleep 1 }.not_to raise_error
  end

  context 'io closed from outside gets unregistered' do
    ['@rd', '@wr'].each { |name|
      it name do
	io = instance_variable_get name
	IOLoop.register_read(io) {}
	io.close
	sleep 2
	expect(IOLoop.registered? io).to eq false
      end
    }
  end

  it '#select_if_value_present returns hash keys for items having values' do
    hash = { 1 => :foo, 2 => nil }
    expect(IOLoop.select_if_value_present hash).to eq [1]
  end

  context 'when call_callback_on_unregister_read is' do
    it 'true, calls callback with nil on unregister' do
      called_with_nil = false
      IOLoop.register_read(@rd) { |buf| called_with_nil = true unless buf }
      IOLoop.unregister_read @rd
      expect(called_with_nil).to eq true
    end

    it "false, don't call callback with nil on unregister" do
      IOLoop.call_callback_on_unregister_read = false
      called_with_nil = false
      IOLoop.register_read(@rd) { |buf| called_with_nil = true unless buf }
      IOLoop.unregister_read @rd
      expect(called_with_nil).to eq false
    end
  end
end
