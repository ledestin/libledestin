RUBY_VER := 1.9.1
LIB := /usr/local/lib/site_ruby/$(RUBY_VER)/ledestin

gem:
	gem build *.gemspec

deb:
	equivs-build libledestin-ruby

install:
	install -d $(LIB)
	install -m644 *.rb $(LIB)

uninstall:
	rm -r $(LIB)
